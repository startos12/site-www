<aside class="alert alert-warning" markdown="1">
  **Dart 2 note:** Pub in Dart 2 no longer supports `pub build`, `pub serve`, or transformers.
  Instead, use the new [build system,](https://github.com/dart-lang/build)
  which includes the **build_runner** tool.
  For details, see the
  [build_runner README](https://github.com/dart-lang/build/blob/master/build_runner/README.md#build_runner) or the
  [web-specific build_runner documentation.]({{site.dev-webdev}}/tools/build_runner)
</aside>
