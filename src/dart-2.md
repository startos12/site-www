---
layout: default
permalink: /dart-2
title: Dart 2 Updates
description: How Dart 2 is different from Dart 1.x, and how you can convert your code to work with Dart 2.
---

For the latest information on Dart 2 and migrating your code,
see [**www.dartlang.org/dart-2**.]({{site.www}}/dart-2)
